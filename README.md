#Job ads SPA
This is the frontend part of the React and Node.js SPA.

##Techniques/libraries used
React, Redux, Redux-Thunk, validate.js, react-dropzone, Bootstrap 4.

##Get started
Run npm install followed by npm start.

##How it works
The user will first be directed to the landing page containing all the job openings.
At first visit, there will be no ads to display. Create ads in the backend by pressing the "Create ads" button - then press the Refresh button.
Assuming the database has been setup we are now looking at Job oepnings in Stockholm. There is a sorting going on here based on which ads have the lowest number of applications registered - 
They will be prioritized in the list of ads.

Pressing apply will direct the user to the application place where a contact form needs to be filled followed by pressing the "Continue" button in order for the appication to be sent in successfully.
Validation is done with validate.js and basic JS to check empty strings and string lengths.
