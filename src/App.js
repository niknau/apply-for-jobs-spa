import React, { Component } from 'react';
import { connect } from 'react-redux';
import LandingPage from './components/LandingPage';
import Application from './components/Application';
import ApplicationDone from './components/ApplicationDone';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

class App extends Component {
  render() {
    const { currentPage } = this.props.sharedState;
    return (
      <div className="container">
        <div className="col-xs-12 col-md-6 content">
          {currentPage === 'LandingPage' &&
            <LandingPage />}
          {currentPage === 'Application' &&
            <Application />}
          {currentPage === 'ApplicationDone' &&
            <ApplicationDone />}
        </div>
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    sharedState: state.sharedState
  }
};

export default connect(mapStateToProps)(App);

