import * as helpers from '../helpers'
import * as orchestration from '../orchestration/orchestration'

const createAds = () => {
  return (dispatch) => {
    orchestration.createAds().then(response => {
      return response.json()
    }).then(data => {
      const ads = helpers.sortAds(JSON.parse(data.body));
      dispatch({ type: 'HYDRATE_ADS', payload: ads })
    })
  }
}

const getAds = () => {
  return (dispatch) => {
    orchestration.getAds().then(response => {
      return response.json()
    }).then(data => {
      const ads = helpers.sortAds(JSON.parse(data.body));
      dispatch({ type: 'HYDRATE_ADS', payload: ads })
    })
  }
}

const apply = (ad) => ({
  type: 'APPLY',
  payload: ad
});

const applicationFinished = (ad, candidate) => ({
  type: 'APPLICATION_FINISHED',
  payload: { ad, candidate }
});

const changePage = (page) => ({
  type: 'CHANGE_PAGE',
  payload: page
});



export {
  createAds,
  getAds,
  apply,
  changePage,
  applicationFinished
};