const sortAds = (ads) => ads.sort((a, b) => a.applications.length - b.applications.length);

export {
  sortAds
}
