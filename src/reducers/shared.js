const sharedState = (state = {
  currentPage: 'LandingPage',
  ads: []
}, action) => {
  let newState = { ...state };
  switch (action.type) {
    case 'CHANGE_PAGE':
      return { ...newState, currentPage: action.payload }
    case 'HYDRATE_ADS':
      return { ...newState, ads: action.payload }
    default:
      return newState;
  }
};
export default sharedState;