const applicationState = (state = {
  ad: {},
  finishedApplication: {},
  updatedAd: {},
  candidate: {}
}, action) => {
  let newState = { ...state };
  switch (action.type) {
    case 'APPLY':
      return { ...newState, ad: action.payload }
    case 'APPLICATION_FINISHED':
      return { ...newState, candidate: action.payload.candidate, updatedAd: action.payload.ad }
    default:
      return newState;
  }
};
export default applicationState;