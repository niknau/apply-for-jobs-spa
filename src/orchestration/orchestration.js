const BASE_URL = 'http://localhost:3001';

const createAds = () => {
  return fetch(`${BASE_URL}/create_ads`, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    }
  })
}

const getAds = () => {
  return fetch(`${BASE_URL}/get_ads`, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    }
  })
};

const registerApplication = (applicationData) => {
  return fetch(`${BASE_URL}/register_application`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(applicationData)
  })
};

export {
  createAds,
  getAds,
  registerApplication
}