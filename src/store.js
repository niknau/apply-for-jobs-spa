import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import sharedState from './reducers/shared';
import applicationState from './reducers/application';

export default createStore(
  combineReducers({
    sharedState,
    applicationState
  }),
  applyMiddleware(thunk)
);