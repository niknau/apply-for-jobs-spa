import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/actions';

const ApplicationDone = (props) => {
  const { candidate, updatedAd } = props.applicationState;
  const { applications } = updatedAd;

  return (
    <div>
      <nav aria-label="breadcrumb">
        <ol className="breadcrumb">
          <li className="breadcrumb-item" onClick={() => props.changePage('LandingPage')}><p>Job openings</p></li>
          <li className="breadcrumb-item" onClick={() => props.changePage('Application')}><p>Application</p></li>
          <li className="breadcrumb-item active"><p>Application done</p></li>
        </ol>
      </nav>
      <div className="card">
        <div className="card-body">
          <h4 className="card-title">Thank you, {candidate.firstName}, for your application</h4>
        </div>
      </div>
      <div className="card" >
        <div className="card-body">
          <h4 className="card-title">"{updatedAd.title}" has been populated with an additional candidate. There are now {applications.length} applications registered on this ad.</h4>
        </div>
      </div>
      <button className="btn btn-primary" onClick={() => props.changePage('LandingPage')}>Go back to start</button>
    </div>)
}

const mapDispatchToProps = dispatch => {
  return {
    changePage: (page) => dispatch(actions.changePage(page))
  }
}

const mapStateToProps = state => {
  return {
    applicationState: state.applicationState
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationDone);