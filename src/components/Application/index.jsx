import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/actions';
import { validate } from 'validate.js';
import Dropzone from 'react-dropzone'
import * as orchestration from '../../orchestration/orchestration';

class Application extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      phoneNumber: '',
      errors: { firstName: false, lastName: false, email: false, phoneNumber: false },
      isFormInvalid: true
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.validateField = this.validateField.bind(this);
  }

  handleChange(event) {
    const { name, value } = event.target;
    let { errors, isFormInvalid } = { ...this.state };
    const fieldIsInvalid = this.isFieldInValid(name, value);
    if (!fieldIsInvalid) {
      errors[name] = fieldIsInvalid;
    }
    if (Object.values(errors).indexOf(true) === -1) {
      isFormInvalid = false;
    }
    this.setState({ [name]: value, isFormInvalid, errors });
  }

  validateField(event) {
    const { name, value } = event.target;
    let { errors, isFormInvalid } = this.state;
    errors[name] = this.isFieldInValid(name, value);
    if (Object.values(errors).indexOf(true) !== -1) {
      isFormInvalid = true;
    } else {
      isFormInvalid = false;
    }
    this.setState({ isFormInvalid });
  }

  isFieldInValid(name, value) {
    if (value === '') {
      return true
    }
    switch (name) {
      case 'email':
        return (validate({ from: value }, { from: { email: true } })) !== undefined;
      case 'phoneNumber': {
        const phoneNumberLength = value.length;
        if (phoneNumberLength > 10 || phoneNumberLength < 4) {
          return true
        }
        break;
      }
      default:
        break;
    }
  }

  handleSubmit(event) {
    event.preventDefault();

    const { firstName, lastName, email, phoneNumber } = this.state;
    const adId = this.props.ad._id;
    const candidate = {
      firstName, lastName, email, phoneNumber
    };
    const applicationData = {
      candidate,
      adId
    };

    orchestration.registerApplication(applicationData).then(response => {
      return response.json()
    }).then(data => {
      const applicationResponse = data;
      if (applicationResponse.success) {
        this.props.applicationFinished(applicationResponse.ad, candidate);
        this.props.changePage('ApplicationDone');
        return;
      }
      this.setState({ error: 'Something went wrong. Please try again later.' });
    })
  }

  render() {
    const { ad } = this.props;
    const { description, title } = ad;
    const { firstName, lastName, email, phoneNumber, isFormInvalid, errors } = this.state;

    return (
      <div>
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item" onClick={() => this.props.changePage('LandingPage')}><p>Job openings</p></li>
            <li className="breadcrumb-item active"><p>Application</p></li>
          </ol>
        </nav>
        <div className="card" >
          <div className="card-body">
            <h4 className="card-title">{title}</h4>
            <p className="card-text">{description}</p>
          </div>
        </div>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label>Email address</label>
            <input onChange={this.handleChange} onBlur={this.validateField} type="email" value={email} name="email" className="form-control" id="inputEmail" aria-describedby="emailHelp" placeholder="Enter email" />
            {errors.email && <span className="error">Invalid field.</span>}
            <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
          </div>
          <div className="form-group">
            <label>First name</label>
            <input onChange={this.handleChange} onBlur={this.validateField} type="text" value={firstName} name="firstName" className="form-control" aria-describedby="firstName" placeholder="First name" />
            {errors.firstName && <span className="error">Invalid field.</span>}
          </div>
          <div className="form-group">
            <label>Last name</label>
            <input onChange={this.handleChange} onBlur={this.validateField} type="text" value={lastName} name="lastName" className="form-control" id="inputLastName" placeholder="Last name" />
            {errors.lastName && <span className="error">Invalid field.</span>}
          </div>
          <div className="form-group">
            <label>Phone number</label>
            <input onChange={this.handleChange} onBlur={this.validateField} type="text" value={phoneNumber} name="phoneNumber" className="form-control" id="inputPhoneNumber" placeholder="Phone number" />
            {errors.phoneNumber && <span className="error">Invalid field.</span>}
          </div>
          <div className="form-group">
            <label>Upload profile image</label>
            <Dropzone onDrop={this.onDrop}>
              {({ getRootProps, getInputProps, isDragActive }) => {
                return (
                  <div
                    {...getRootProps()}
                  >
                    <input {...getInputProps()} />
                    {
                      isDragActive ?
                        <p>Drop files here...</p> :
                        <p>Try dropping some files here, or click to select files to upload.</p>
                    }
                  </div>
                )
              }}
            </Dropzone>
          </div>
          <button disabled={isFormInvalid} type="submit" className="btn btn-success">Continue</button>
        </form>
      </div>);
  }
}

const mapDispatchToProps = dispatch => {
  return {
    apply: (ad) => dispatch(actions.apply(ad)),
    changePage: (page) => dispatch(actions.changePage(page)),
    applicationFinished: (ad, candidate) => dispatch(actions.applicationFinished(ad, candidate))
  }
}

const mapStateToProps = state => {
  return {
    ad: state.applicationState.ad
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Application);