import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/actions';

const AdCard = (props) => {
  const { ad } = props;
  const { description, title } = ad;
  const handleAdClick = (ad) => {
    props.apply(ad);
    props.changePage('Application');
  }

  return (
    <div className="card">
      <div className="card-body">
        <h4 className="card-title">{title}</h4>
        <p className="card-text">{description}</p>
        <button onClick={() => handleAdClick(ad)} href="#" className="btn btn-primary">Apply now</button>
      </div>
    </div>)
}

const mapDispatchToProps = dispatch => {
  return {
    apply: (ad) => dispatch(actions.apply(ad)),
    changePage: (page) => dispatch(actions.changePage(page))
  }
}

export default connect(null, mapDispatchToProps)(AdCard);