import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/actions';

import AdCard from './AdCard';

class LandingPage extends Component {
  componentDidMount() {
    this.props.getAds();
  };

  render() {
    const { ads } = this.props.sharedState;
    const adsToDisplay = ads.map(ad => <AdCard ad={ad} key={ad._id} />);
    const hasNoAds = adsToDisplay.length === 0;
    return (
      <div>
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active"><p>Job openings</p></li>
          </ol>
        </nav>
        <h2 className="page-title">Job Openings in Stockholm</h2>
        {hasNoAds ?
          <div>
            <p>At the moment there are no ads to show.</p>
            <button className="btn btn-success" onClick={this.props.createAds}>Create ads.</button>
            <button className="btn btn-success refresh-btn" onClick={this.props.getAds}>Refresh.</button>
          </div>
          :
          adsToDisplay
        }

      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    sharedState: state.sharedState
  }
}

const mapDispatchToProps = dispatch => {
  return {
    changePage: (page) => dispatch(actions.changePage(page)),
    createAds: () => dispatch(actions.createAds()),
    getAds: () => dispatch(actions.getAds())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LandingPage);